# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi

  attr_reader :towers
  WINNING_TOWER = [3,2,1]
  TOWER_HASH = {1 =>0, 2 =>1, 3=>2}

  def initialize
    @towers = [[3,2,1],[],[]]
  end

  def play
    move_counter=0
    until won?
      puts "Moves used so far: #{move_counter}"
      render
      puts "From which tower would you like to remove a disc?"
      from_tower = TOWER_HASH[gets.chomp.to_i]
      puts " "
      puts "Great! Which tower would you like to put that disc in?"
      to_tower = TOWER_HASH[gets.chomp.to_i]
      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
        move_counter+=1
      else
        puts "Move was invalid - try again!"
      end
    end
    you_won(move_counter)
  end

  def you_won(move_counter)
    puts "Congratulations! You completed the game in #{move_counter} moves!"
    render
  end

  def render
    discs = Hash.new
    tower_label = " Tower 1 "+" Tower 2 "+" Tower 3 "
    discs[0] = "    |    "
    discs[1] = "   [1]   "
    discs[2] = "  [ 2 ]  "
    discs[3] = " [  3  ] "
    i=2
    puts "                  "
    puts "--CURRENT TOWERS--"
    puts "                  "
    while i>=0
      @towers.each do |tower|
        if tower[i].nil?
          print discs[0]
        else
          print discs[tower[i]]
        end
      end
      puts " "
      i-=1
    end
    puts " "
    puts tower_label
  end

  def move(from_tower,to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    # invalid conditions:
    # the numbers are wrong
    if from_tower.nil? || to_tower.nil?
      puts "Please choose numbers from 1 to 3!"
      return false
    end
    # same from and to values
    if from_tower == to_tower
      puts "Putting the disc in the same tower you took it from doesn't help you!"
      return false
    end
    # there are no discs in from_tower1
    if @towers[from_tower].empty?
      puts "There are no discs in Tower #{from_tower}"
      return false
    end
    # to_tower is full
    if @towers[to_tower].length==3
      puts "Tower #{to_tower} is full!"
      return false
    end
    # last value in to_tower is smaller than the last value in from_tower
    if @towers[to_tower].empty? == false
      if @towers[from_tower].last > @towers[to_tower].last
        puts "You can't put a disc on top of a smaller disc!"
        return false
      end
    end
    true
  end

  def won?
    @towers[1..-1].any? { |tower| tower == WINNING_TOWER }
  end

end
